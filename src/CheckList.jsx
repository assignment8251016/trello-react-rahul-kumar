import axios from 'axios'
import { useEffect, useState } from 'react'

import { Button, Form } from 'react-bootstrap'
import { ListGroup, Card } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'
import CheckItemsList from './CheckItemsList'

const Key = '0ce662f80ec23139bc77e965ebd1d8a5'
const Token =
    'ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937'

function getCheckItems(checklistId, setCheckitems) {
    axios
        .get(
            `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${Key}&token=${Token}`
        )
        .then((response) => setCheckitems(response.data))
        .then((data) => console.log(data))
}

function createCheckItem(checklistId, name, checkitems, setCheckitems) {
    axios
        .post(
            `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${name}&key=${Key}&token=${Token}`
        )
        .then((response) => setCheckitems([...checkitems, response.data]))

    console.log('chekc lost', checkitems)
}

function deleteCheckItem(checklistId, checkitemId, checkitems, setCheckitems) {
    axios
        .delete(
            `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${Key}&token=${Token}`
        )
        .then((response) => console.log(response.data))

    setCheckitems(
        checkitems.filter((checkitem) => checkitem.id !== checkitemId)
    )
}

function updateCheckitem(checkitemId, cardId, state) {
    axios.put(
        `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=${
            state ? 'complete' : 'incomplete'
        }&key=${Key}&token=${Token}`
    )
}

export default function CheckList({ checklist, deleteCheckList }) {
    const [checkitems, setCheckitems] = useState([])
    const [text, setText] = useState(undefined)
    useEffect(() => {
        getCheckItems(checklist.id, setCheckitems)
    }, [])
    console.log('in view', checklist)

    return (
        <ListGroup.Item>
            <Card className="checkitem">
                <FontAwesomeIcon
                    icon={faCircleXmark}
                    onClick={deleteCheckList}
                />
                <Card.Title className="mb-4">{checklist.name}</Card.Title>

                <CheckItemsList
                    checkitems={checkitems}
                    deleteCheckItem={(checkitemId) =>
                        deleteCheckItem(
                            checklist.id,
                            checkitemId,
                            checkitems,
                            setCheckitems
                        )
                    }
                    updateCheckitem={(checkitemId, state) =>
                        updateCheckitem(checkitemId, checklist.idCard, state)
                    }
                />

                <Form.Control
                    className="add-check-item"
                    type="text"
                    id="inputPassword5"
                    placeholder="Press 'Enter' to add check item"
                    onKeyUp={(e) => {
                        setText(e.target.value)
                        if (e.key === 'Enter') {
                            createCheckItem(
                                checklist.id,
                                text,
                                checkitems,
                                setCheckitems
                            )
                        }
                    }}
                />
            </Card>
        </ListGroup.Item>
    )
}
