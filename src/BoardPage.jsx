import { useEffect, useState } from 'react'
import { Button, Card, Container, Row } from 'react-bootstrap'
import { useParams, useLocation } from 'react-router-dom'
import axios from 'axios'

import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import List from './List'
import Error from './Error/Error'
const Key = '0ce662f80ec23139bc77e965ebd1d8a5'
const Token =
    'ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937'

async function archiveList(lists, setLists, listId) {
    setLists(lists.filter((list) => list.id !== listId))
    return axios.put(
        `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${Key}&token=${Token}`
    )
}
async function getList(id, setLists, setError) {
    console.log('getLists()')
    axios
        .get(
            `https://api.trello.com/1/boards/${id}/lists?key=${Key}&token=${Token}`
        )
        .then((res) => {
            setLists(res.data)
            console.log('new list', res.data)
        })
        .catch((error) => {
            setError(true)
        })
}

function addAnotherList(event, name, id, lists, setLists) {
    axios
        .post(
            `https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${Key}&token=${Token}`
        )
        .then((response) => setLists([...lists, response.data]))
        .then(() => console.log('new list added'))
}

export default function BoardPage() {
    console.log('board page ...')
    const [error, setError] = useState(false)
    const { state } = useLocation()
    const { image } = state || {
        image: 'https://www.bing.com/th?id=OHR.PlayfulHumpback_EN-IN6301739594_1920x1080.webp',
    }
    const { id } = useParams()

    const [lists, setLists] = useState(undefined)
    const [name, setName] = useState(undefined)

    useEffect(() => {
        console.log('setting lists!')
        getList(id, setLists, setError)
    }, [])

    console.log(error)
    return (
        <Container fluid style={{ backgroundImage: `url(${image})` }}>
            {!error ? (
                <Row style={{ flexWrap: 'nowrap' }}>
                    {lists !== undefined &&
                        lists.map((list, index) => {
                            return (
                                <List
                                    key={list.id}
                                    list={{ ...list, image }}
                                    boardId={id}
                                    archiveList={() =>
                                        archiveList(lists, setLists, list.id)
                                    }
                                    getLists={() => getList(id, setLists)}
                                />
                            )
                        })}

                    <InputGroup
                        className="mb-3 dragable-container"
                        style={{
                            marginTop: '10px',
                            width: '300px',
                            height: '50px',
                        }}
                    >
                        <Form.Control
                            placeholder="New list name"
                            aria-label="list-name"
                            aria-describedby="basic-addon2"
                            onChange={(e) => setName(e.target.value)}
                        />
                        <Button
                            variant="outline-secondary"
                            id="button-addon2"
                            onClick={(event) => {
                                if (name) {
                                    addAnotherList(
                                        event,
                                        name,
                                        id,
                                        lists,
                                        setLists
                                    )
                                }
                            }}
                        >
                            + New List
                        </Button>
                    </InputGroup>
                </Row>
            ) : (
                <Error />
            )}
        </Container>
    )
}
