import { Link } from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import {Form, FormControl, Button} from 'react-bootstrap';
export default function Navigation() {

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Link to="/">
          <Navbar.Brand ><img src='https://a.trellocdn.com/prgb/assets/d947df93bc055849898e.gif' /></Navbar.Brand>
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Form inline className="d-flex">
          <FormControl type="text" placeholder="Search" className="me-2" />
          <Button variant="outline-success">Search</Button>
        </Form>
      </Container>
      
    </Navbar>
  )
}