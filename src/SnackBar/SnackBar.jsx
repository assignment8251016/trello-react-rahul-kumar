import React, { useState } from 'react'
import { ToastContainer } from 'react-bootstrap'
import Toast from 'react-bootstrap/Toast'

function AutohideMessage({ message }) {
    const [show, setShow] = useState(false)

    return (
        <div
            aria-live="polite"
            aria-atomic="true"
            className="bg-dark position-relative"
            style={{ minHeight: '240px' }}
        >
            <ToastContainer
                className="p-3"
                position={position}
                style={{ zIndex: 1 }}
            >
                <Toast
                    onClose={() => setShow(false)}
                    show={show}
                    delay={2000}
                    autohide
                >
                    <Toast.Header>
                        <img
                            src="holder.js/20x20?text=%20"
                            className="rounded me-2"
                            alt=""
                        />
                        <strong className="me-auto">Bootstrap</strong>
                        <small>11 mins ago</small>
                    </Toast.Header>
                    <Toast.Body>{message}</Toast.Body>
                </Toast>
            </ToastContainer>
        </div>
    )
}

export default AutohideMessage
