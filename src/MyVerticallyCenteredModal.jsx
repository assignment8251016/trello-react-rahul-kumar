import { list } from 'fontawesome'
import Button from 'react-bootstrap/Button'
import { Modal, Form, Card } from 'react-bootstrap'
import InputGroup from 'react-bootstrap/InputGroup'
import CheckList from './CheckList'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faListCheck } from '@fortawesome/free-solid-svg-icons'

import axios from 'axios'
import { useEffect, useState } from 'react'

const Key = '0ce662f80ec23139bc77e965ebd1d8a5'
const Token =
    'ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937'

function getChecklists(idCard, setCheckList) {
    axios
        .get(
            `https://api.trello.com/1/checklists/${idCard}?key=${Key}&token=${Token}`
        )
        .then((response) => setCheckList(response.data))
        .then(() => console.log('checlists fetched!'))
}

function createCheckList(idCard, text, checklists, setCheckList) {
    console.log(text)
    axios
        .post(
            `https://api.trello.com/1/checklists?idCard=${idCard}&name=${text}&key=${Key}&token=${Token}`
        )
        .then((response) => {
            console.log('checlists: ', checklists)
            console.log('checlists 2: ', response.data)
            setCheckList([...checklists, response.data])
        })
}

function deleteCheckList(checklistId, checklists, setCheckList) {
    console.log(
        'deleting checklist',
        checklistId,
        checklists.find((ch) => ch.id === checklistId)
    )
    axios.delete(
        `https://api.trello.com/1/checklists/${checklistId}?key=${Key}&token=${Token}`
    )
    setCheckList(checklists.filter((checklist) => checklist.id !== checklistId))
}

function handleCheckList(e, setText) {
    setText(e.target.value)
}

export default function MyVerticallyCenteredModal({
    show,
    onHide,
    card,
    listName,
    checklists,
    setCheckList,
}) {
    const [text, setText] = useState(undefined)

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <FontAwesomeIcon icon={faListCheck} />
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">{listName}</li>

                        <li class="breadcrumb-item active" aria-current="page">
                            {' '}
                            {card.name}
                        </li>
                    </ol>
                </nav>
            </Modal.Header>
            <Modal.Body>
                <Card align-content-between gap-3>
                    {checklists &&
                        checklists.map((checklist) => (
                            <CheckList
                                key={checklist.id}
                                checklist={checklist}
                                deleteCheckList={() =>
                                    deleteCheckList(
                                        checklist.id,
                                        checklists,
                                        setCheckList
                                    )
                                }
                            />
                        ))}
                </Card>
                <InputGroup>
                    <Form.Control
                        onChange={(e) => handleCheckList(e, setText)}
                        placeholder="Check list name"
                    />
                    <Button
                        variant="outline-secondary"
                        onClick={(e) =>
                            createCheckList(
                                card.id,
                                text,
                                checklists,
                                setCheckList
                            )
                        }
                    >
                        Add
                    </Button>
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}
