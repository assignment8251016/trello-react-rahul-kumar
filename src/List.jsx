import { useEffect, useState } from "react";
import axios from "axios";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEraser,faCircleXmark, faBoxArchive } from '@fortawesome/free-solid-svg-icons'
import { Col, Row, Card, ListGroup, InputGroup, Form } from "react-bootstrap";
import CCard from "./CCard";

const Key = "0ce662f80ec23139bc77e965ebd1d8a5";
const Token = "ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937"


function handleAddCardToList(event, list, name, setCards, cards) {
    if (event.key === "Enter") {
        axios.post(`https://api.trello.com/1/cards?idList=${list.id}&name=${name}&key=${Key}&token=${Token}`)
        .then((response) => setCards([...cards,response.data]))
        .then(()=> event.target.value = "")
       
    }
    // console.log(event.target.value);

}


async function getCards(list, setCards) {

    console.log("all cards on list:", list.id, "name:")
    axios.get(`https://api.trello.com/1/lists/${list.id}/cards?key=${Key}&token=${Token}`).then(res => setCards(res.data));

}




async function deleteCard(cardId){
    axios.delete(`https://api.trello.com/1/cards/${cardId}?key=${Key}&token=${Token}`).then((response)=>{
        console.log(response);
    })
}


export default function List({ list, archiveList}) {
    const [cards, setCards] = useState([]);

    // console.log("out current list", list)

    useEffect(()=>{
        // console.log("current list", list)
        getCards(list, setCards);
    }, [])


    return (
        <Card className='list'>
            <Card.Header className='list-name d-flex justify-content-between'>
                {list.name}
                <FontAwesomeIcon  onClick={()=>{
                    archiveList();
                }} icon={faBoxArchive} />

            </Card.Header>
            <ListGroup container variant="flush">
                {
                    cards?.map(card => 
                        <CCard list={list} card={card} setCards={setCards} deleteCard={deleteCard} cards={cards}/>
                    )
                }

                <ListGroup.Item>
                    <InputGroup size="sm" className="mb-3" style={{ width: "90%", marginLeft: "10px" }}>
                        <Form.Control style={{fontSize: "1.4rem"}} placeholder="Press 'Enter' to add new card" aria-label="Small" aria-describedby="inputGroup-sizing-sm" onKeyDown={(e)=>handleAddCardToList(e, list, e.target.value, setCards, cards)} />
                    </InputGroup>
                </ListGroup.Item>
            </ListGroup>
        </Card>
    );
}