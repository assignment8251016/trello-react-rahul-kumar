import { Container, Row } from 'react-bootstrap'
import { Form, InputGroup, Button } from 'react-bootstrap'
import Board from './Board'
import { useState } from 'react'

export default function ({ boards, addAnotherBoard, error }) {
    const [name, setName] = useState(undefined)
    return (
        <Container fluid style={{ backgroundColor: 'rgb(244, 244, 244)' }}>
            <Row>
                {boards ? (
                    boards.map((board) => (
                        <Board
                            image={board.prefs.backgroundImage}
                            name={board.name}
                            boardId={board.id}
                            color={board.prefs.backgroundColor}
                        />
                    ))
                ) : (
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            opacity: '.8',
                            marginTop: '20%',
                        }}
                    >
                        <h1 style={{ color: '#800000' }}>Loading....</h1>
                    </div>
                )}
                <InputGroup
                    className="mb-3 dragable-container"
                    style={{
                        marginTop: '10px',
                        width: '300px',
                        height: '50px',
                    }}
                >
                    <Form.Control
                        placeholder="New list name"
                        aria-label="list-name"
                        aria-describedby="basic-addon2"
                        onChange={(e) => {
                            setName(e.target.value)
                        }}
                    />
                    <Button
                        variant="outline-secondary"
                        id="button-addon2"
                        onClick={(event) => {
                            if (name) {
                                addAnotherBoard(name)
                            }
                        }}
                    >
                        + New List
                    </Button>
                </InputGroup>
            </Row>
        </Container>
    )
}
