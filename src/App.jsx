import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/dist/css/bootstrap.min.css'

import ContainerBoards from './ContainerBoards'
import BoardPage from './BoardPage'
import axios from 'axios'

import { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'

const Key = '0ce662f80ec23139bc77e965ebd1d8a5'
const Token =
    'ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937'

function addAnotherBoard(name, boards, setBoards, setError) {
    axios
        .post(
            `https://api.trello.com/1/boards/?name=${name}&key=${Key}&token=${Token}`
        )
        .then((response) => setBoards([...boards, response.data]))
        .catch((err) => console.log(err))
}

function App() {
    const [boards, setBoards] = useState(undefined)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        axios
            .get(
                `https://api.trello.com/1/members/me/boards?prefs_background=sky&key=${Key}&token=${Token}`
            )
            .then((res) => setBoards(res.data))
            .then(() => setLoading(false))
            .catch((err) => setLoading(true))
    }, [])

    console.log(boards)

    return (
        <>
            {!loading ? (
                <Routes>
                    <Route
                        path="/"
                        element={
                            <ContainerBoards
                                boards={boards}
                                addAnotherBoard={(name) =>
                                    addAnotherBoard(name, boards, setBoards)
                                }
                            />
                        }
                    />
                    <Route path="/boards/:id" element={<BoardPage />} />
                </Routes>
            ) : (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        opacity: '.8',
                        marginTop: '20%',
                    }}
                >
                    <h1 style={{ color: '#800000' }}>Loading ...</h1>
                </div>
            )}
        </>
    )
}

export default App
