import { useState } from 'react'
import axios from 'axios'

import MyVerticallyCenteredModal from './MyVerticallyCenteredModal'
import { ListGroup } from 'react-bootstrap'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'

const Key = '0ce662f80ec23139bc77e965ebd1d8a5'
const Token =
    'ATTA26546f7d2de12c65bcbc5cbe5b472381e7864c99cf47c70d36030e2195629e1bDBDE7937'

function getChecklists(idCard, setCheckList) {
    axios
        .get(
            `https://api.trello.com/1/cards/${idCard}/checklists?key=${Key}&token=${Token}`
        )
        .then((response) => setCheckList(response.data))
}

export default function CCard({ list, card, deleteCard, cards, setCards }) {
    const [modalShow, setModalShow] = useState(false)
    const [checklists, setCheckList] = useState(undefined)
    console.log(modalShow)
    return (
        <>
            <ListGroup.Item
                key={card.id}
                onClick={() => {
                    console.log(card)
                    getChecklists(card.id, setCheckList)
                }}
            >
                <div className="custom-card-row">
                    <p onClick={() => setModalShow(true)}>{card.name}</p>
                    <FontAwesomeIcon
                        icon={faCircleXmark}
                        onClick={() => {
                            deleteCard(card.id)
                            setCards(cards.filter((c) => c.id !== card.id))
                        }}
                    />
                </div>
            </ListGroup.Item>

            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                card={card}
                listName={list.name}
                checklists={checklists}
                setCheckList={setCheckList}
            />
        </>
    )
}
