import { Form, Card, Button, Row } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons'
import { useEffect, useState } from 'react'

export default function CheckItem({
    checkitem,
    deleteCheckItem,
    updateCheckitem,
}) {
    const [state, setState] = useState(
        checkitem.state === 'complete' ? true : false
    )
    useEffect(() => {
        updateCheckitem(checkitem.id, state)
    }, [state])
    return (
        <>
            <Form.Check
                type={`checkbox`}
                label={checkitem.name}
                checked={state}
                onChange={(e) => {
                    console.log('checking')
                    // setState(e.target.checked)
                    if (e.target.checked) {
                        setState(true)
                    } else {
                        setState(false)
                    }
                }}
            />

            <FontAwesomeIcon
                icon={faCircleXmark}
                onClick={() => {
                    deleteCheckItem(checkitem.id)
                }}
            />
        </>
    )
}
